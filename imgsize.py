# Cups size translation
translatedSizes = {
    'b7':'3_5x5',
    'w288h432':'4x6',
    'w360h504':'5x7',
    'w432h576':'6x8'
}

# Maximum and minimum side sizes, regardless of orientation
size_pixels = {
    '300x300_3_5x5_max':1548,
    '300x300_3_5x5_min':1088,
    '300x300_4x6_max':1844,
    '300x300_4x6_min':1240,
    '300x300_5x7_max':2138,
    '300x300_5x7_min':1548,
    '300x300_6x8_max':2436,
    '300x300_6x8_min':1844,
    '300x600_3_5x5_max':2176,
    '300x600_3_5x5_min':1548,
    '300x600_4x6_max':2480,
    '300x600_4x6_min':1844,
    '300x600_5x7_max':4276,
    '300x600_5x7_min':1548,
    '300x600_6x8_max':4872,
    '300x600_6x8_min':1844,
    '600x600_3_5x5_max':3096,
    '600x600_3_5x5_min':2176,
    '600x600_4x6_max':3688,
    '600x600_4x6_min':2480,
    '600x600_5x7_max':4276,
    '600x600_5x7_min':3096,
    '600x600_6x8_max':4872,
    '600x600_6x8_min':3688
}

def pixels(res_size):
    maxp = size_pixels[res_size + '_max']
    minp = size_pixels[res_size + '_min']
    return [maxp, minp]


def translateSize(media):
    size = ''
    if media in translatedSizes:
        size = translatedSizes[media]
    return size

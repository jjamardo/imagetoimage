#!/usr/bin/python
import sys
import ConfigParser
import cups
from wand.image import Image
import imgsize
from datetime import datetime
import logging
import os
import subprocess
#from shutil import copyfile
import shutil

## Auxiliary Functios ##
logging.basicConfig( format = '%(asctime)s  %(levelname)-10s %(processName)s  %(name)s %(message)s', datefmt =  "%Y-%m-%d-%H-%M-%S" )

def save_to_pendrive(filename, device, folder):
    if os.path.exists(device):
        try:
            subprocess.Popen(['mount',device,folder])
        except Exception as e:
            print(e)
        try:
            dst = folder + '/' + datetime.now().strftime("%Y%m%d")
            if not os.path.exists(dst):
                os.makedirs(dst)
                shutil.copy2(filename, dst)
                #subprocess.Popen(['umount',folder])
        except Exception as e:
            print(e)
            #print(datetime.now(), 'CANNOT SAVE TO: ', device)
    else:
        print('NO DEVICE TO SAVE')


def aspect_ratio(width, height):
    return float("{0:.2f}".format(float(width) / float(height)))


def cups_size(media):
    idx_ = media.index('_')
    media = media[idx_ + 1:]
    idx_ = media.index('_')
    media = media[:idx_]
    return imgsize.translateSize(media)


def cups_resolution(resolution):
    return str(resolution[0]) + 'x' + str(resolution[1])


## Main execution ##

if __name__ == '__main__':
    # Check arguments
    if len(sys.argv) == 2:
        filename = sys.argv[1]
    else:
        print('No filename argument passed')
        sys.exit()

    # Load configuration
    config = ConfigParser.RawConfigParser()
    config.read('default.cfg')
    watermark_enabled = bool(config.get('SYSTEM', 'watermark'))
    backup_usb_enabled = bool(config.get('SYSTEM', 'backup_usb'))
    usb_device = config.get('SYSTEM', 'backup_usb_device')
    usb_folder = config.get('SYSTEM', 'backup_usb_folder')

    # CUPS default configuration
    conn = cups.Connection()
    printer = conn.getDefault()
    attributes = conn.getPrinterAttributes(printer, requested_attributes=['printer-resolution-default', 'media-default'])
    resolution = cups_resolution(attributes['printer-resolution-default'])
    size = cups_size(attributes['media-default'])

    # If backup usb is enabled, save a copy of the original file.
    if (backup_usb_enabled):
        save_to_pendrive(filename, usb_device, usb_folder)
    quit()

    # ImageMagick image transformation.
    try:
        with Image(filename=filename) as img:
            res_size = resolution + '_' + size
            img.auto_orient()
            pixels = imgsize.pixels(res_size)
            frame_width = 0
            frame_height = 0
            orientation = ''
            if (img.width > img.height):
                # Landscape
                print('image orientation landscape')
                orientation = 'l'
                frame_width = pixels[0]
                frame_height = pixels[1]
            else:
                # Portrait
                print('image orientation portrait')
                orientation = 'p'
                frame_width = pixels[1]
                frame_height = pixels[0]

            # Watermark configuration key format:
            #   watermark_[resolution]_[size]_[orientation]
            # ex: watermark_300x300_3_5x5_l (landscape=l, portrait=p)
            option = 'watermark_' + res_size + '_' + orientation
            frame_filename = config.get('SYSTEM', option)

            frame_aspect_ratio = aspect_ratio(frame_width, frame_height)
            image_aspect_ratio = aspect_ratio(img.width, img.height)

            print(datetime.now(), 'frame file ' + frame_filename)
            print(datetime.now(), 'frame width ' + str(frame_width))
            print('frame height ' + str(frame_height))
            print('frame aspect ratio ' + str(frame_aspect_ratio))
            print('image aspect ratio ' + str(image_aspect_ratio))

            if (image_aspect_ratio < frame_aspect_ratio):
                img.transform(resize="%d" % frame_width)
                img.crop(width=frame_width, height=frame_height, gravity='center')
                if (watermark_enabled):
                    with Image(filename=frame_filename) as frame:
                        img.composite(frame, left=0, top=0)
            else:
                img.transform(resize="x%d" % frame_height)
                img.crop(width=frame_width, height=frame_height, gravity='center')
                if (watermark_enabled):
                    with Image(filename=frame_filename) as frame:
                        img.composite(frame, left=0, top=0)

            img.save(filename=filename + ".modif.png")
            if (backup_usb_enabled):
                save_to_pendrive('modif_' + filaname, usb_device, usb_folder)

    except Exception as e:
        print(e)



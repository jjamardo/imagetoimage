#!/usr/bin/python
from __future__ import print_function
import sys
import os
import cups
import json

global process_name

def log(message):
    print(process_name + ': ' + message, file=sys.stderr)


## Main execution ##

if __name__ == '__main__':
    global process_name
    process_name = sys.argv[0]

    try:
        # Check arguments
        if len(sys.argv) != 3:
            log('Invalid number of arguments passed')
            # Let cupsd and filters continue processing normally
            sys.exit(0)

        host = sys.argv[1]
        uuid = sys.argv[2]
        conn = cups.Connection()
        remove = False
        current_id = -1
        for job_id in conn.getJobs():
            attributes = conn.getJobAttributes(job_id)
            if ('job-originating-host-name' in attributes):
                job_host = attributes['job-originating-host-name']
            else:
                log('No job-originating-host-name found for job:' + str(job_id) + ', assuming localhost')
                job_host = 'localhost'
            job_uuid = attributes['job-uuid']
            if (host == job_host and job_uuid != uuid):
                log('A job for host:' + host + ' is already in the queue')
                remove = True
            if (job_uuid == uuid):
                log('Job ' + str(job_id) + ' found for uuid: ' + uuid)
                current_id = job_id

        if (remove):
            if (current_id == -1):
                log('No job id found for uuid:' + uuid)
                sys.exit(0)
            log('Job ' + str(current_id) + ' will be canceled')
            conn.cancelJob(current_id, True)
            # After cancelJob this script will be terminated from cupsd, so nothing from here wil be executed.
    except Exception as e:
        log(str(e))

    # This job can be processed.
    sys.exit(0)

